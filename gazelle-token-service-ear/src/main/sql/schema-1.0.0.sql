--
-- PostgreSQL database dump
--

-- Dumped from database version 13.6 (Ubuntu 13.6-0ubuntu0.21.10.1)
-- Dumped by pg_dump version 13.6 (Ubuntu 13.6-0ubuntu0.21.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: gazelle_api_token; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gazelle_api_token
(
    value           character varying(255)      NOT NULL,
    creation_date   timestamp without time zone NOT NULL,
    expiration_date timestamp without time zone NOT NULL,
    organization    character varying(255)      NOT NULL,
    username        character varying(255)      NOT NULL
);


ALTER TABLE public.gazelle_api_token
    OWNER TO gazelle;

--
-- Name: gazelle_token_preference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gazelle_token_preference
(
    preference_id  character varying(255) NOT NULL,
    token_duration integer
);


ALTER TABLE public.gazelle_token_preference
    OWNER TO gazelle;

--
-- Name: gazelle_token_roles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.gazelle_token_roles
(
    id    character varying(255) NOT NULL,
    roles character varying(255)
);


ALTER TABLE public.gazelle_token_roles
    OWNER TO gazelle;

--
-- Name: gazelle_api_token gazelle_api_token_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gazelle_api_token
    ADD CONSTRAINT gazelle_api_token_pkey PRIMARY KEY (value);


--
-- Name: gazelle_token_preference gazelle_token_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gazelle_token_preference
    ADD CONSTRAINT gazelle_token_preference_pkey PRIMARY KEY (preference_id);


--
-- Name: gazelle_token_roles fkd7wxdqmvaewvv3ufsg52o5d7r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.gazelle_token_roles
    ADD CONSTRAINT fkd7wxdqmvaewvv3ufsg52o5d7r FOREIGN KEY (id) REFERENCES public.gazelle_api_token (value) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--