/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.ihm.TokenBean;
import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 26/09/2022
 */
public class TokenBeanTest {

    private final TokenBean bean = mock(TokenBean.class);
    private Token token1;
    private Token token2;
    private List<Token> tokens;

    @Inject
    private GetPrincipal principal;

    @BeforeEach
    public void setUp() {
        token1 = Token.builder()
                .withValue("csdfdsfdkslmfds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().plusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        token2 = Token.builder()
                .withValue("lkfdjslkfjds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().minusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        List<Token> tokens = new ArrayList<>();
        tokens.add(token1);
        tokens.add(token2);
    }

    @Test
    void getUserTokensTest() {
        when(bean.getUserTokens()).thenReturn(tokens);
        List<Token> tokenList = bean.getUserTokens();
        Assertions.assertEquals(tokenList, tokens);
    }

    @Test
    void getActiveTokenTest() throws UserNotFoundException {
        when(bean.getUniqueActiveToken()).thenReturn(token1);
        Token token = bean.getUniqueActiveToken();
        Assertions.assertNotNull(token);
    }

    @Test
    void createTokenTest() throws UserNotFoundException {
        when(bean.getUniqueActiveToken()).thenReturn(token1);
        bean.createToken();
        Assertions.assertNotNull(token1);
    }

    @Test
    void getDeactivateTokenTest() throws UserNotFoundException {
        when(bean.getUniqueActiveToken()).thenReturn(token2);
        bean.getDeactivateToken();
        Assertions.assertNotNull(token2);
    }

    @Test
    void isExpiredTest() {
        boolean active = bean.isExpired(token1.getValue());
        boolean inactive = bean.isExpired(token2.getValue());
        Assertions.assertFalse(active);
        Assertions.assertFalse(inactive);
    }

}
