import net.ihe.gazelle.token.application.model.EConstantValue;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.ihm.AdminBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 26/09/2022
 */
public class AdminBeanTest {

    private final AdminBean bean = mock(AdminBean.class);
    private Preference preference;
    private Token inactiveToken1;

    @BeforeEach
    public void setUp() {
        preference = Preference.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(200)
                .build();
        inactiveToken1 = Token.builder()
                .withValue("lkfdjslkfjds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().minusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
    }

    @Test
    void isExpiredTest() {
        when(bean.isExpired(inactiveToken1.getValue())).thenReturn(true);
        boolean expired = bean.isExpired(inactiveToken1.getValue());
        Assertions.assertTrue(expired);
    }

    @Test
    void getPreferenceTest() {
        when(bean.getDuration()).thenReturn(preference.getTokenDuration());
        int pref = bean.getDuration();
        Assertions.assertEquals(200, pref);
    }

}