/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import net.ihe.gazelle.token.interlay.ihm.MenuBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 26/09/2022
 */
public class MenuBeanTest {

    private final MenuBean bean = mock(MenuBean.class);

    @Test
    void getNowTest() {
        when(bean.getNow()).thenReturn("");
        String date = bean.getNow();
        Assertions.assertNotNull(date);
    }

    @Test
    void getVersionTest() {
        when(bean.getVersion()).thenReturn("");
        String version = bean.getVersion();
        Assertions.assertNotNull(version);
    }
}