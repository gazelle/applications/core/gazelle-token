
package net.ihe.gazelle.token.interlay.ihm.model;

import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import org.apache.commons.lang3.StringUtils;
import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 05/07/2022
 */
@Named
@LogAudited
@RequestScoped
public class GetPrincipal implements Serializable {

    private static final String ROLE_NAME = "role_name";
    private boolean loggedIn = false;
    private String username;
    private String organization; // institution_keyword
    private List<String> roles = new ArrayList<>(); // role_name
    private Subject subject = new Subject();
    private String password;
    private AttributePrincipal principal;

    @PostConstruct
    public void init() {
        principal = getPrincipal();
        if (principal != null) {
            loggedIn = true;
            username = principal.getName();
            organization = (String) principal.getAttributes().get("institution_keyword");
            roles = parseRoles((String) principal.getAttributes().get(ROLE_NAME));
        }
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public String getUsername() {
        return this.username;
    }

    public String getOrganization() {
        return organization;
    }

    public List<String> getRoles() {
        return roles;
    }


    public Subject getSubject() {
        return subject;
    }


    public String getPassword() {
        return password;
    }

    private AttributePrincipal getPrincipal() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context == null) {
            return null;
        }
        final HttpServletRequest request = (HttpServletRequest) context.getExternalContext()
                .getRequest();
        final HttpSession session = request.getSession(false);
        Assertion assertion = (Assertion) (session == null ? request
                .getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION) : session
                .getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION));
        return assertion != null ? assertion.getPrincipal() : null;
    }

    private List<String> parseRoles(String roles) {
        List<String> rolesList = new ArrayList<>();
        final StringTokenizer st = new StringTokenizer(roles, "[,]");
        String role;
        while (st.hasMoreElements()) {
            role = (String) st.nextElement();
            role = StringUtils.trimToNull(role);
            if (role != null) {
                rolesList.add(role);
            }
        }
        return rolesList;
    }

    public boolean checkAlreadyLogged() throws IOException {
        principal = getPrincipal();
        if (principal != null) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/token.xhtml");
            return true;
        }
        return false;
    }

    public boolean isAdmin() {
        roles = parseRoles((String) principal.getAttributes().get(ROLE_NAME));
        for (String role : roles) {
            if (role.trim().contains("admin_role")) {
                return true;
            }
        }
        return false;
    }

    public boolean checkIsAdmin() throws IOException {
        if (!isAdmin()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
            return true;
        }
        return false;
    }

}

