
package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 27/06/2022
 */
@Named
@SessionScoped
public class UserBean implements Serializable {

    private GetPrincipal principal;

    private boolean logout = false;

    public boolean isLogout() {
        return logout;
    }

    private String username;
    private String organization;

    public String getUsername() {
        return username;
    }

    public String getOrganization() {
        return organization;
    }

    @PostConstruct
    public void init() {
        if (principal != null) {
            username = principal.getUsername();
            organization = principal.getOrganization();
        }
    }

    public void checkAlreadyLoggedIn() throws IOException {
        if (principal.isLoggedIn()) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/gazelle-token-service/");
        }
    }

}
