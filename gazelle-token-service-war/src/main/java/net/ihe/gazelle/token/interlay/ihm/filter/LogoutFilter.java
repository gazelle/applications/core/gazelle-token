package net.ihe.gazelle.token.interlay.ihm.filter;

import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;
import org.jasig.cas.client.Protocol;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
@Named("/cas/logout")
public class LogoutFilter extends AbstractCasFilter {

    private static final Logger LOG = LoggerFactory.getLogger(LogoutFilter.class);
    @Inject
    private GetPrincipal principal;
    private String casServerUrl;
    private String applicationUrl;
    @Inject
    public HttpSession session;

    public LogoutFilter() {
        super(Protocol.CAS2);
    }

    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    public String getCasServerUrl() {
        return casServerUrl;
    }

    public void setCasServerUrl(String casServerUrl) {
        this.casServerUrl = casServerUrl;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
        StringBuilder logoutUrlBuilder = new StringBuilder();
        try {
            logoutUrlBuilder.append(getCasServerUrl());
            logoutUrlBuilder.append("/logout");
            logout(session);
            if (getApplicationUrl() != null) {
                logoutUrlBuilder.append("?service=");
                final String encodedKey = URLEncoder.encode(getApplicationUrl(), StandardCharsets.UTF_8.toString());
                logoutUrlBuilder.append(encodedKey);
                LOG.debug("url to reach : " + logoutUrlBuilder);
            }
            ((HttpServletResponse) servletResponse).sendRedirect(logoutUrlBuilder.toString());
        } catch (IOException e) {
            LOG.error("" + e.getMessage());
        }
    }

    @Override
    public void init() {
        super.init();
        setCasServerUrl(getString(ConfigurationKeys.CAS_SERVER_URL_PREFIX));
        reloadParameters();
    }

    private void reloadParameters() {
        setApplicationUrl(getString(ConfigurationKeys.SERVICE));
    }

    public String logout(HttpSession session){
        session.invalidate();
        return "redirect:/gazelle-token-service/";
    }



}
