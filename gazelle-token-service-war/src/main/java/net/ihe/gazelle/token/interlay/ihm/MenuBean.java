
package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.application.utils.DateFormatter;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 01/07/2022
 */
@Named
@LogAudited
@RequestScoped
public class MenuBean implements Serializable {

    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    public MenuBean(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @Inject
    private DateFormatter formatter;

    private final LocalDateTime now = LocalDateTime.now();

    public String getNow() {
        return formatter.formatYearToString(now);
    }

    public String getVersion() {
        return this.serviceFactory.getServiceFactory().getPropertiesService().getVersion();
    }
}