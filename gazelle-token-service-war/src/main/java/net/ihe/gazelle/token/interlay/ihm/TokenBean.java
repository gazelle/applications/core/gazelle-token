
package net.ihe.gazelle.token.interlay.ihm;

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.domain.entity.Token;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;
import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 04/07/2022
 */
@RequestScoped
@Named
@LogAudited
public class TokenBean implements Serializable {

    private static final long serialVersionUID = 1905122041950251207L;

    private final TokenInfrastructureFactory serviceFactory;

    @Inject
    private GetPrincipal principal;

    @Inject
    public TokenBean(TokenInfrastructureFactory serviceFactory) {
        this.serviceFactory = serviceFactory;
    }

    @PostConstruct
    public void init() {
        Optional<Token> activeToken;
        try {
            activeToken = getActiveToken();
        } catch (UserNotFoundException e) {
            activeToken = Optional.empty();
        }
    }

    public List<Token> getUserTokens() {
        String username = principal.getUsername();
        return serviceFactory.getServiceFactory().getTokenService().getAllTokenPerUser(username);
    }
    @Transactional
    public void createToken() throws UserNotFoundException {
        String username = principal.getUsername();
        String organization = principal.getOrganization();
        List<String> roles = principal.getRoles();
        serviceFactory.getServiceFactory().getTokenService().saveToken(username, organization, roles);
    }

    public Token getUniqueActiveToken() throws UserNotFoundException {
        return getActiveToken().get();
    }

    public boolean isActiveToken() throws UserNotFoundException {
        return getActiveToken().isPresent();
    }

    private Optional<Token> getActiveToken() throws UserNotFoundException {
        String username = principal.getUsername();
        return this.serviceFactory.getServiceFactory().getTokenService().getUniqueActiveToken(username);
    }

    @Transactional
    public void getDeactivateToken() throws UserNotFoundException {
        Token token = this.getUniqueActiveToken();
        this.serviceFactory.getServiceFactory().getTokenService().deactivateValidToken(token.getValue());
    }

    public boolean isExpired(String value) {
        Token token = this.serviceFactory.getServiceFactory().getTokenService().findByValue(value);
        return !token.getExpirationDate().isBefore(LocalDateTime.now());
    }
    @Transactional
    public void removeByUserExpiredTokens() {
        String username = principal.getUsername();
        this.serviceFactory.getServiceFactory().getTokenService().deleteOldTokensByUser(username);
    }

    public String getFormatDate(LocalDateTime dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        return dateTime.format(formatter);
    }

}
