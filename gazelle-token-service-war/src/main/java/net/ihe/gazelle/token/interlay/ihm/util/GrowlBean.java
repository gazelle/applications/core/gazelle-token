
package net.ihe.gazelle.token.interlay.ihm.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 11/07/2022
 */
@Named
public class GrowlBean {

    public void resultCreateToken() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growlForm:growl", new FacesMessage("",  "Le token à été correctement généré !") );
    }

    public void resultCopyToken() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growlForm:copy", new FacesMessage("",  "Le token à été copié dans le presse-papier !") );
    }

    public void resultDeactivateToken() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growlForm:deactivate", new FacesMessage("",  "Le token à été désactivé !") );
    }

    public void resultPurge() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growlForm:purge", new FacesMessage("",  "Les tokens expirés ont été suprimés !") );
    }

    public void error() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("growlForm:error", new FacesMessage("",  "Une erreur c'est produite !") );
    }

}
