
package net.ihe.gazelle.token.interlay.ihm.filter;


import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.ihm.model.GetPrincipal;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 12/07/2022
 */
@LogAudited
@Named
//@WebFilter(filterName = "AuthFilter", urlPatterns = { "admin.xhtml" })
public class AuthFilter {

    @Inject
    private GetPrincipal principal;



}
