<?xml version='1.0' encoding='UTF-8'?>
<!--
 ~ Gazelle Token Service is an Api for the Gazelle Test Bed
 ~ Copyright (C) 2006-2022 IHE
 ~ mailto :claude DOT lusseau AT kereval DOT com
 ~
 ~ See the NOTICE file distributed with this work for additional information
 ~ regarding copyright ownership.  This code is licensed
 ~ to you under the Apache License, Version 2.0 (the
 ~ "License"); you may not use this file except in compliance
 ~ with the License.  You may obtain a copy of the License at
 ~
 ~   http://www.apache.org/licenses/LICENSE-2.0
 ~
 ~ Unless required by applicable law or agreed to in writing,
 ~ software distributed under the License is distributed on an
 ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 ~ KIND, either express or implied.  See the License for the
 ~ specific language governing permissions and limitations
 ~ under the License.
-->
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">

    <display-name>Gazelle Token Service</display-name>


    <mime-mapping>
        <extension>xhtml</extension>
        <mime-type>text/html</mime-type>
    </mime-mapping>

    <!-- ########################################
    ################ FACES CONFIG ###############
    ######################################### -->

    <!-- The default is Bootstrap, the valid values are a Bootswach Theme name (lowercase) or "custom".
         If custom is chosen, you will have to provide your custom CSS in the "other" folder. -->
    <context-param>
        <param-name>BootsFaces_THEME</param-name>
        <param-value>cerulean</param-value>
    </context-param>
    <!-- The BootsFaces_USETHEME context-param controls the loading of the bootstrap-theme.css -->
    <!-- The default is false, when set to true the bootstrap-theme.css will be loaded -->
    <context-param>
        <param-name>BootsFaces_USETHEME</param-name>
        <param-value>cerulean</param-value>
    </context-param>

    <context-param>
        <param-name>javax.faces.STATE_SAVING_METHOD</param-name>
        <param-value>server</param-value>
    </context-param>

    <context-param>
        <param-name>javax.faces.DEFAULT_SUFFIX</param-name>
        <param-value>*.xhtml</param-value>
    </context-param>
    <context-param>
        <param-name>javax.faces.FACELETS_REFRESH_PERIOD</param-name>
        <param-value>0</param-value>
    </context-param>

    <context-param>
        <param-name>javax.faces.validator.ENABLE_VALIDATE_WHOLE_BEAN</param-name>
        <param-value>true</param-value>
    </context-param>

    <context-param>
        <param-name>javax.faces.ENABLE_CDI_RESOLVER_CHAIN</param-name>
        <param-value>true</param-value>
    </context-param>


    <servlet>
        <servlet-name>Faces Servlet</servlet-name>
        <servlet-class>javax.faces.webapp.FacesServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet-mapping>
        <servlet-name>Faces Servlet</servlet-name>
        <url-pattern>*.xhtml</url-pattern>
    </servlet-mapping>


    <session-config>
        <session-timeout>20</session-timeout>
    </session-config>

    <welcome-file-list>
        <welcome-file>/index.xhtml</welcome-file>
    </welcome-file-list>



    <!-- ########################################
    ################ FRONT CONFIG ###############
    ######################################### -->

    <context-param>
        <param-name>net.bootsfaces.get_jquery_from_cdn</param-name>
        <param-value>false</param-value>
    </context-param>
    <context-param>
        <param-name>net.bootsfaces.get_jqueryui_from_cdn</param-name>
        <param-value>false</param-value>
    </context-param>
    <context-param>
        <param-name>net.bootsfaces.get_bootstrap_from_cdn</param-name>
        <param-value>true</param-value>
    </context-param>
    <context-param>
        <param-name>net.bootsfaces.get_fontawesome_from_cdn</param-name>
        <param-value>true</param-value>
    </context-param>

    <error-page>
        <error-code>403</error-code>
        <location>/error/error403.xhtml</location>
    </error-page>
    <error-page>
        <error-code>404</error-code>
        <location>/error/error404.xhtml</location>
    </error-page>
    <error-page>
        <error-code>500</error-code>
        <location>/error/error500.xhtml</location>
    </error-page>


    <!-- ########################################
    ################# CAS CLIENT ################
    ######################################### -->

    <context-param>
        <param-name>configurationStrategy</param-name>
        <param-value>PROPERTY_FILE</param-value>
    </context-param>
    <context-param>
        <param-name>configFileLocation</param-name>
        <param-value>/opt/gazelle/cas/gazelle-token-service.properties</param-value>
    </context-param>

    <filter>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <filter-class>org.jasig.cas.client.session.SingleSignOutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Single Sign Out Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
    <listener>
        <listener-class>org.jasig.cas.client.session.SingleSignOutHttpSessionListener</listener-class>
    </listener>

    <filter>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <filter-class>org.jasig.cas.client.authentication.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS Authentication Filter</filter-name>
        <url-pattern>/cas/login</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <filter-class>net.ihe.gazelle.token.interlay.ihm.filter.LogoutFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Gazelle CAS logout filter</filter-name>
        <url-pattern>/cas/logout</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Validation Filter</filter-name>
        <filter-class>org.jasig.cas.client.validation.Cas30ProxyReceivingTicketValidationFilter</filter-class>
        <init-param>
            <param-name>redirectAfterValidation</param-name>
            <param-value>true</param-value>
        </init-param>

    </filter>
    <filter-mapping>
        <filter-name>CAS Validation Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.HttpServletRequestWrapperFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS HttpServletRequest Wrapper Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>CAS Assertion Thread Local Filter</filter-name>
        <filter-class>org.jasig.cas.client.util.AssertionThreadLocalFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>CAS Assertion Thread Local Filter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <filter>
        <filter-name>Restricted Area</filter-name>
        <filter-class>org.jasig.cas.client.authentication.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>Restricted Area</filter-name>
        <url-pattern>/admin.xhtml</url-pattern>
    </filter-mapping>

    <!-- ########################################
    ################# REST CONFIG ###############
    ######################################### -->

    <context-param>
        <param-name>resteasy.preferJacksonOverJsonB</param-name>
        <param-value>false</param-value>
    </context-param>

</web-app>
