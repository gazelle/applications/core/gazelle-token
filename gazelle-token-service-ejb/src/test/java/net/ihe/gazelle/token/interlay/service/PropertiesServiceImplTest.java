/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.token.interlay.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 25/09/2022
 */
class PropertiesServiceImplTest {

    private final PropertiesServiceImpl propertiesService = new PropertiesServiceImpl();
    @Test
    void getVersionTest() {
        String version = propertiesService.getVersion();
        Assertions.assertNotNull(version);
    }
}
