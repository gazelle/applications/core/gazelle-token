
package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.exception.UserNotFoundException;
import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 25/08/2022
 */
class TokenDaoImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "GTZ_TEST";
    private static EntityManagerFactory factory;
    private EntityManager entityManager;
    private TokenDaoImpl dao;
    private static TokenEntity activeToken1;
    private static TokenEntity inactiveToken1;
    private static List<TokenEntity> doesTokens;

    @BeforeAll
    public static void setUp() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);

    }
    @AfterAll
    public static void closeAndDropDatabase() {
        // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
        factory.close();
    }

    @BeforeEach
    public void initializeEntityManager() {
        entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        dao = new TokenDaoImpl(entityManager);
        activeToken1 = TokenEntity.builder()
                .withValue("csdfdsfdkslmfds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().plusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        inactiveToken1 = TokenEntity.builder()
                .withValue("lkfdjslkfjds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().minusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        doesTokens = new ArrayList<>();
        doesTokens.add(activeToken1);
        doesTokens.add(inactiveToken1);
    }

    @AfterEach
    public void closeEntityManager() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    void testCreate() {
        dao.create(activeToken1);
        TokenEntity token = dao.getTokenByValue(activeToken1.getValue());
        Assertions.assertEquals(activeToken1.getValue(), token.getValue());
    }


    @Test
    void testDeleteOldTokens() {
        doesTokens.forEach(token -> {
            if (Objects.equals(token.getUsername(), "jdoe") && token.getExpirationDate().isBefore(LocalDateTime.now())) {
                dao.deleteOldTokens(token.getValue());
            }
        });
        TokenEntity token = dao.getTokenByValue(inactiveToken1.getValue());
        Assertions.assertNull(token);
    }

    @Test
    void testGetAllTokens() {
        List<TokenEntity> list = dao.getTokensPerOwner("jdoe");
        Assertions.assertNotNull(list);
    }

    @Test
    void testDeactivateValidToken() {
        dao.deactivateValidToken(activeToken1.getValue(), LocalDateTime.now().minusMinutes(2));
        Assertions.assertFalse(activeToken1.getExpirationDate().isBefore(LocalDateTime.now()));
    }
}