package net.ihe.gazelle.token.interlay.mapper;

import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.domain.entity.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 27/09/2022
 */
public class TokenMapperTest {

    private final TokenMapperImpl mapper = new TokenMapperImpl();

    @Test
    void tokenEntityToPreferenceTest() {
        TokenEntity tokenEntity = TokenEntity.builder().build();
        Token token = mapper.tokenEntityToToken(tokenEntity);
        Assertions.assertNotNull(token);
    }

    @Test
    void tokenToTokenEntityTest() {
        Token token = Token.builder().build();
        TokenEntity tokenEntity = mapper.tokenToTokenEntity(token);
        Assertions.assertNotNull(tokenEntity);
    }
}
