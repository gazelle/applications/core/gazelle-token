
package net.ihe.gazelle.token.application.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/08/2022
 */
public class PreferenceEntityTest {

    @Test
    void shouldGetAllPreferenceInformation() {
        PreferenceEntity preference = PreferenceEntity.builder()
                .withTokenDuration(365)
                .build();
        Assertions.assertEquals(preference.getTokenDuration(), 365);
    }

    @Test
    void shouldNotBuildWithoutValues() {
        PreferenceEntity preference = PreferenceEntity.builder()
                .withTokenDuration(0)
                .build();
        Assertions.assertNotEquals(365, preference.getTokenDuration());
    }
}
