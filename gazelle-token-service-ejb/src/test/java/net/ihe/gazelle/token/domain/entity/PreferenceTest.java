
package net.ihe.gazelle.token.domain.entity;

import net.ihe.gazelle.token.domain.entity.Preference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token-service
 * @date 09/06/2022
 */
class PreferenceTest {

    @Test
    void shouldGetAllPreferenceInformation() {
        Preference preference = Preference.builder()
                .withTokenDuration(365)
                .build();
        Assertions.assertEquals(365, preference.getTokenDuration());
    }

    @Test
    void shouldNotBuildWithoutValues() {
        Preference preference = Preference.builder()
                .withTokenDuration(0)
                .build();
        Assertions.assertNotEquals(365, preference.getTokenDuration());
    }
}
