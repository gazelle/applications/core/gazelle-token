
package net.ihe.gazelle.token.application.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project GazelleTokenService
 * @date 17/06/2022
 */
public class TokenEntityTest {

    @Test
    void shouldGetAllTokenInformation() {
        Assertions.assertNotNull(TokenEntity.builder()
                .withValue("test")
                .withUsername("mfortiche")
                .withOrganization("kereval")
                .withCreationDate(LocalDateTime.now())
                .withExpirationDate(LocalDateTime.now())
                .withRoles(new ArrayList<>())
                .build());
    }

    @Test
    void shouldNotBuildWithoutValues() {
        List<String> roles = new ArrayList<>();
        Assertions.assertNotNull(TokenEntity.builder()
                .withValue("")
                .withUsername("")
                .withOrganization("")
                .withCreationDate(null)
                .withExpirationDate(null)
                .withRoles(roles));
    }
}
