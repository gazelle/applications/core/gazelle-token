/**
 * Gazelle Token Service is an Api for the Gazelle Test Bed
 * Copyright (C) 2006-2022 IHE
 * mailto :claude DOT lusseau AT kereval DOT com
 * <p>
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.token.interlay.controller;

import net.ihe.gazelle.token.application.utils.DateFormatter;
import net.ihe.gazelle.token.domain.entity.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 26/09/2022
 */
public class TokenRestControllerTest {

    private final TokenRestController controller = mock(TokenRestController.class);
    private Token activeToken1;
    private Token inactiveToken1;
    private List<Token> doesTokens;
    @Mock
    private Response response;
    @Inject
    private DateFormatter dateFormatter;

    @BeforeEach
    public void init() {
        activeToken1 = Token.builder()
                .withValue("csdfdsfdkslmfds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().plusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        inactiveToken1 = Token.builder()
                .withValue("lkfdjslkfjds")
                .withUsername("jdoe")
                .withOrganization("IHE")
                .withCreationDate(LocalDateTime.now().minusDays(300))
                .withExpirationDate(LocalDateTime.now().minusDays(30))
                .withRoles(List.of("admin_roles"))
                .build();
        doesTokens = new ArrayList<>();
        doesTokens.add(activeToken1);
        doesTokens.add(inactiveToken1);
    }

    @Test
    void isValidTokenTest() throws Exception {
        Response test;
        test = controller.isValidToken(inactiveToken1.getValue());
        Assertions.assertSame(response, test);
        Response test2;
        test2 = controller.isValidToken(activeToken1.getValue());
        Assertions.assertSame(response, test2);
    }

    @Test
    void badRequestTest() throws Exception {
        Response test;
        test = controller.badRequest();
        Assertions.assertSame(response, test);
    }
}
