
package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.model.EConstantValue;
import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 12/07/2022
 */
class PreferenceDaoImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "GTZ_TEST";
    private static EntityManagerFactory factory;
    private EntityManager entityManager;
    private PreferenceDaoImpl dao;
    private PreferenceEntity preference;
    private PreferenceEntity preferenceEntity;

    @BeforeAll
    public static void setUp() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
    }

    @AfterAll
    public static void closeAndDropDatabase() {
        // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
        factory.close();
    }

    @BeforeEach
    public void initializeEntityManager() {
        entityManager = factory.createEntityManager();
        entityManager.getTransaction().begin();
        dao = new PreferenceDaoImpl(entityManager);
        preference = PreferenceEntity.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(200)
                .build();
    }

    @AfterEach
    public void closeEntityManager() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    void testCreate() {
        dao.save(preference);
        PreferenceEntity preferenceEntity = dao.getPreference(preference.getId());
        Assertions.assertEquals(preference.getTokenDuration(), preferenceEntity.getTokenDuration());
    }

    @Test
    void testUpdate() throws DaoException {
        PreferenceEntity updatePref = PreferenceEntity.builder()
                .withId(EConstantValue.getPreferenceId())
                .withTokenDuration(300)
                .build();
        try {
            dao.update(updatePref);
        } catch (DaoException d) {
            throw new DaoException(d);
        }
        Assertions.assertEquals(300, updatePref.getTokenDuration());
    }

}