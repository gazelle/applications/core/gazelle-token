
package net.ihe.gazelle.token.interlay.security;

import javax.inject.Qualifier;
import java.lang.annotation.*;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project GazelleTokenService
 * @date 14/06/2022
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER, ElementType.TYPE})
@Documented
public @interface TokenGenerationQualifier {
}
