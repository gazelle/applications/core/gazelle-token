
package net.ihe.gazelle.token.application.model;

import net.ihe.gazelle.token.application.exception.TokenValidationException;
import net.ihe.gazelle.token.domain.entity.Token;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token-service
 * @date 08/06/2022
 */
public class TokenValidator {

    protected TokenValidator() {
    }

    public static void validateCreateToken(final Token token) {
        if (token == null) throw new TokenValidationException("Token should not be null");
        if (token.getValue() == null) throw new TokenValidationException("Value should not be null");
        if (token.getUsername() == null) throw new TokenValidationException("Username should not be null");
        if (token.getOrganization() == null) throw new TokenValidationException("Organisation should not be null");
        if (token.getCreationDate() == null) throw new TokenValidationException("Creation date should not be null");
        if (token.getExpirationDate() == null) throw new TokenValidationException("Expiration date should not be null");
        if (token.getRoles() == null) throw new TokenValidationException("Roles should not be null");

    }
}
