
package net.ihe.gazelle.token.interlay.controller;

import net.ihe.gazelle.token.interlay.factory.TokenInfrastructureFactory;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 07/07/2022
 */
@Path("/v1/authn/user")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
@LocalBean
@Named("TokenRestController")
public class TokenRestController  {
    @Inject
    private TokenInfrastructureFactory serviceFactory;

    @GET
    @Path("{value}")
    public Response isValidToken(@PathParam("value") String value) {
        return this.serviceFactory.getServiceFactory().getRestService().isValidToken(value);
    }

    @GET
    @Path("/")
    public Response badRequest() {
        return this.serviceFactory.getServiceFactory().getRestService().badRequest();
    }
}
