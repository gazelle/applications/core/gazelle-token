
package net.ihe.gazelle.token.application.model;

import javax.inject.Named;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 15/07/2022
 */
@Named("EConstantValue")
public class EConstantValue {
    private static final String PREFERENCE_ID = "KEREVAL_ADMIN_APP";
    public static String getPreferenceId() {
        return PREFERENCE_ID;
    }
}
