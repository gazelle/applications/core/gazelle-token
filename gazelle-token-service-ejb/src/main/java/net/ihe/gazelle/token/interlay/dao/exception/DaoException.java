
package net.ihe.gazelle.token.interlay.dao.exception;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token-service
 * @date 10/06/2022
 */
public class DaoException extends Exception {

    public DaoException(final DaoException message) {
        super(message);
    }
}