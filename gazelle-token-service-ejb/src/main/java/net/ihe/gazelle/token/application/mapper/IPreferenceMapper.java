
package net.ihe.gazelle.token.application.mapper;

import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.domain.entity.Preference;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 14/07/2022
 */
public interface IPreferenceMapper {

    Preference PreferenceEntityToPreference(PreferenceEntity preferenceEntity);
    PreferenceEntity preferenceToPreferenceEntity(Preference preference);
}
