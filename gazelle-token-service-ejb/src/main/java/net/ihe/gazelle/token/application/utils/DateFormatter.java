
package net.ihe.gazelle.token.application.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project GazelleTokenService
 * @date 17/06/2022
 */
public class DateFormatter {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    private final DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy");

    public String formatDateToString(LocalDateTime dateTime){
        return dateTime.format(formatter);
    }

    public String formatYearToString(LocalDateTime dateTime){
        return dateTime.format(yearFormatter);
    }

}
