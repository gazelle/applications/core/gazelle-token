
package net.ihe.gazelle.token.application.service;

import javax.ws.rs.core.Response;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
public interface IRestService {
    Response isValidToken(String value);

    Response badRequest();
}
