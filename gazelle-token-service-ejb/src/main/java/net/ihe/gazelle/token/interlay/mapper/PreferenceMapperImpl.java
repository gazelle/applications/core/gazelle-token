
package net.ihe.gazelle.token.interlay.mapper;

import net.ihe.gazelle.token.application.mapper.IPreferenceMapper;
import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.domain.entity.Preference;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;

import javax.inject.Named;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 14/07/2022
 */
@LogAudited
@Named("PreferenceMapper")
public class PreferenceMapperImpl implements IPreferenceMapper {

    @Override
    public Preference PreferenceEntityToPreference(PreferenceEntity preferenceEntity) {
        if (preferenceEntity != null) {
            return Preference.builder()
                    .withId(preferenceEntity.getId())
                    .withTokenDuration(preferenceEntity.getTokenDuration())
                    .build();
        }
        return null;
    }

    @Override
    public PreferenceEntity preferenceToPreferenceEntity(Preference preference) {
        return PreferenceEntity.builder()
                .withId(preference.getId())
                .withTokenDuration(preference.getTokenDuration())
                .build();
    }
}