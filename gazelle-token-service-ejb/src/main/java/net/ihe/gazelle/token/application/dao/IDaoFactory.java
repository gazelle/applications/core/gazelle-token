
package net.ihe.gazelle.token.application.dao;

import net.ihe.gazelle.token.interlay.dao.PreferenceDaoImpl;
import net.ihe.gazelle.token.interlay.dao.TokenDaoImpl;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 25/08/2022
 */
public interface IDaoFactory {

    PreferenceDaoImpl getPreferenceDao();

    TokenDaoImpl getTokenDao();
}
