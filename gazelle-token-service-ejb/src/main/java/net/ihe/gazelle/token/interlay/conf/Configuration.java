package net.ihe.gazelle.token.interlay.conf;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class Configuration {

    private final Properties prop;
    private static Configuration instance;
    private static final String FILE_TO_LOAD = "configuration/project.properties";

    private Configuration() {
        prop = new Properties();
        Thread currentThread = Thread.currentThread();
        ClassLoader contextClassLoader = currentThread.getContextClassLoader();
        InputStream propertiesStream = contextClassLoader.getResourceAsStream(FILE_TO_LOAD);
        if (propertiesStream != null) {
            try {
                prop.load(propertiesStream);
            } catch (Exception e) {
                Logger.getLogger("Properties were not load !");
            }
        }
    }

    public static Configuration getinstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public String getValue(String key) {
        if (prop.containsKey(key)) {
            return prop.getProperty(key);
        } else {
            return "";
        }
    }
}
