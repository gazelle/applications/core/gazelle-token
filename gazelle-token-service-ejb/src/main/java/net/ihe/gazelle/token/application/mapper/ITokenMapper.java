
package net.ihe.gazelle.token.application.mapper;

import net.ihe.gazelle.token.application.model.TokenEntity;
import net.ihe.gazelle.token.domain.entity.Token;

import java.util.List;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project GazelleTokenService
 * @date 14/06/2022
 */
public interface ITokenMapper {

    Token tokenEntityToToken(TokenEntity tokenEntity);

    TokenEntity tokenToTokenEntity(Token token);

    List<Token> tokenEntitiesListToTokenList(List<TokenEntity> tokensEntities);

    List<TokenEntity> tokenListToTokenEntitiesList(List<Token> tokens);

}
