
package net.ihe.gazelle.token.interlay.conf;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token-service
 * @date 25/05/2022
 */
@ApplicationPath("/rest")
public class RestApplicationConfig extends Application {
    // Always empty.
}
