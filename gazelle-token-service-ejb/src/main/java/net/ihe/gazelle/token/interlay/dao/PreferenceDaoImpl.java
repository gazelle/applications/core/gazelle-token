
package net.ihe.gazelle.token.interlay.dao;

import net.ihe.gazelle.token.application.dao.IPreferenceDAO;
import net.ihe.gazelle.token.application.model.PreferenceEntity;
import net.ihe.gazelle.token.interlay.annotation.EntityManagerProducer;
import net.ihe.gazelle.token.interlay.annotation.LogAudited;
import net.ihe.gazelle.token.interlay.dao.exception.DaoException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-token
 * @date 14/07/2022
 */
@LogAudited
@Named("PreferenceDaoImpl")
@Transactional
public class PreferenceDaoImpl implements IPreferenceDAO {

    private final EntityManager entityManager;

    @Inject
    public PreferenceDaoImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public PreferenceEntity getPreference(String id) {
        return this.entityManager.find(PreferenceEntity.class, id);
    }

    @Override
    public void update(PreferenceEntity preference) throws DaoException {
        entityManager.createNamedQuery("PreferenceEntity.updateWithTokenDurationOnly")
                .setParameter("id", preference.getId())
                .setParameter("duration", preference.getTokenDuration())
                .executeUpdate();
    }

    @Override
    public void save(PreferenceEntity preference) {
        entityManager.createNamedQuery("PreferenceEntity.saveWithTokenDurationOnly")
                .setParameter("id", preference.getId())
                .setParameter("duration", preference.getTokenDuration())
                .executeUpdate();
    }
}
