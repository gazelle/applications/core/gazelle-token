
package net.ihe.gazelle.token.application.service;

/**
 * @company KEREVAL
 * @author Claude LUSSEAU
 * @project gazelle-token
 * @date 22/09/2022
 */
public interface IServiceFactory {

    ITokenService getTokenService();

    IPreferenceService getPreferenceService();

    IPropertiesService getPropertiesService();

    IRestService getRestService();
}
